import 'package:app_food_choice/modal_fit.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class FoodForm extends StatefulWidget {
  const FoodForm({super.key, required this.title});

  final String title;

  @override
  State<FoodForm> createState() => _FoodFormState();
}

class _FoodFormState extends State<FoodForm> {
  final _scrollController = ScrollController();

  var _foodList = ["짜장면", "짬뽕", "햄버거", "샤브샤브", "에그드랍", "팬케이크", "서브웨이", "닭갈비",
  "볶음밥", "순두부찌개", "핫도그", "마라탕", "제육볶음", "김치찌개", "떡볶이", "돈까스", "쫄면", "가츠동",
  "초밥", "우동", "파스타", "라면", "리조또", "삼각김밥", "피자", "설렁탕", "순대국", "추어탕", "텐동",
  "된장찌개", "생선구이", "만두", "사케동", "냉면", "김밥"];
  String _foodResult = "";
  String _inputFoodName = "";

  void getFood() {
    int resultIndex = Random().nextInt(_foodList.length);

    setState(() {
        _foodResult = "오늘 점심 메뉴는 " + _foodList[resultIndex] + " 어때요?";
    });
  }

  void addFood() {
    setState(() {
      _foodList.add(_inputFoodName);
    });
  }

  void delFood(int idx) {
    setState(() {
      _foodList.removeAt(idx);
    });
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
          child: OutlinedButton(
              onPressed: () {
                getFood();
              },
              child: Text("골라줘")
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(30, 0, 30, 20),
          alignment: Alignment.center,
          color: Color.fromRGBO(255, 33, 216, 90),
          child: Text(_foodResult),
        ),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(), //리스트 안의 리스트라 스크롤 허용하지 않게하기위해서.
          shrinkWrap: true,
          itemCount: _foodList.length,
          itemBuilder: (_, index) => _buildListItem(index, _foodList[index]),
        ),
      ],
    );
  }

  Widget _buildListItem(int idx, String foodName) {
    return ListTile(
      leading: CircleAvatar(
        child: Text('${idx + 1}'),
      ),
      title: Text(
        '$foodName'
      ),
      trailing: const Icon(Icons.delete),
      onTap: () => showMaterialModalBottomSheet(
          expand: false,
          context: context,
          backgroundColor : Colors.transparent,
          builder: (context) => ModalFit(
            callback: () {
              delFood(idx);
              Navigator.of(context).pop();
            },
          ),
      ),
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("음식 추가"),
            content: Container(
              child: TextField(
                decoration: const InputDecoration(
                  labelText: "음식명",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                ),
                onChanged: (val) => _inputFoodName = val,
              ),
            ),
            actions: [
              OutlinedButton(onPressed: () {
                addFood();
                Navigator.of(context).pop();
              }, child: Text("추가하기"),
              ),
              OutlinedButton(onPressed: () {
                Navigator.of(context).pop();
              }, child: Text("닫기"),
              ),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          _showDialog();
        },
      ),
    );
  }
}

